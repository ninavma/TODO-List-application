module TODO.List.application {
    requires javafx.controls;
    requires javafx.fxml;
    opens edu.ntnu.idata1002.prosjekt2021.model to javafx.base;
    opens edu.ntnu.idata1002.prosjekt2021.ui to javafx.graphics, javafx.fxml;
    exports edu.ntnu.idata1002.prosjekt2021.ui;
}