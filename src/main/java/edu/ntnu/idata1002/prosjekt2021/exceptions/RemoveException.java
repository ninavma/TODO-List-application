package edu.ntnu.idata1002.prosjekt2021.exceptions;

  /**
   * Capture a ssn person that failed to match the social security number
   * in patients or employees in departments.
   */
  public class RemoveException extends Exception {

    public RemoveException(String exception) {

      super(exception);
    }
  }