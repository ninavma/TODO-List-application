package edu.ntnu.idata1002.prosjekt2021.ui;

import edu.ntnu.idata1002.prosjekt2021.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.*;

/**
 * Controller for the careplan scene.
 */
public class CarePlanController {

    /**
     * The textfield for entering a journal entry
     * to the nutrition careplan.
     */
    @FXML
    private TextArea nutritionJournalField;

    /**
     * The textarea for displaying the journal entries
     * in the nutrition careplan.
     */
    @FXML
    private TextArea nutritionJournalList;

    /**
     * The textfield for entering a journal entry
     * to the nutrition careplan.
     */
    @FXML
    private TextArea hygieneJournalField;

    /**
     * The textarea for displaying the journal entries
     * in the nutrition careplan.
     */
    @FXML
    private TextArea hygieneJournalList;

    /**
     * The textfield for entering a journal entry
     * to the social and activity careplan.
     */
    @FXML
    private TextArea socialJournalField;

    /**
     * The textarea for displaying the journal entries
     * in the social and activity careplan.
     */
    @FXML
    private TextArea socialJournalList;

    /**
     * The textfield for entering a journal entry
     * to the medication careplan.
     */
    @FXML
    private TextArea medicationJournalField;

    /**
     * The textarea for displaying the journal entries
     * in the medication careplan.
     */
    @FXML
    private TextArea medicationJournalList;

    /**
     * Label showing whether this patient wishes to be
     * saved or not in case of a medical incident.
     */
    @FXML
    private Label doCprLabel;

    /**
     * Tableview displaying the medicationlist.
     */
    @FXML
    private TableView<TodoTask> medicationTableView;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private TableColumn<TodoTask, CheckBox> medicationListColumn;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private TableColumn<TodoTask, String> medicationTimeColumn;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private HBox nutritionTaskNotDoneButtons;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private HBox nutritionTaskDoneButtons;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private HBox hygieneTaskNotDoneButtons;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private HBox hygieneTaskDoneButtons;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private HBox socialTaskNotDoneButtons;

    /**
     * The container for buttons of different operation for the
     * social and activity careplan that are done.
     */
    @FXML
    private HBox socialTaskDoneButtons;

    /**
     * Constant values for how the lists are displayed.
     */
    public enum ButtonList{
        ALL,
        MORNING,
        AFTERNOON,
        EVENING,
        NIGHT
    }

    /**
     * Holds value for how the lists were last displayed.
     */
    private ButtonList buttonList;

    /**
     * Tabpane containing choices for the patients different careplans.
     */
    @FXML
    private TabPane careplanTabPane;

    /**
     * Button that displays all tasks in the selected careplantab.
     */
    @FXML
    private Button allTasksButton;

    /**
     * Button for displaying the tasks that take place in the morning.
     */
    @FXML
    private Button morningButton;

    /**
     * Button for displaying the tasks that take place in the afternoon.
     */
    @FXML
    private Button afternoonButton;

    /**
     * Button for displaying the tasks that take place in the evening.
     */
    @FXML
    private Button eveningButton;

    /**
     * Button for displaying the tasks that take place at night.
     */
    @FXML
    private Button nightButton;

    /**
     * The additional information good to know for the nutrition careplan.
     */
    @FXML
    private TabPane infoplanNutrition;

    /**
     * The additional information good to know for the personal hygiene careplan.
     */
    @FXML
    private TabPane infoplanHygiene;

    /**
     * The additional information good to know for the social and activity careplan.
     */
    @FXML
    private TabPane infoplanSocial;

    /**
     * Displays cave information and a commentary field.
     */
    @FXML
    private TabPane infoplanMedication;

    /**
     * Link to the patientinfo pop up window.
     */
    @FXML
    private Label patientinfoLink;

    /**
     * ListView over nutrition tasks that are not done.
     */
    @FXML
    private ListView<CheckBox> nutritionNotDoneTasksListView;

    /**
     * ListView over nutrition tasks that are done.
     */
    @FXML
    private ListView<CheckBox> nutritionDoneTasksListView;

    /**
     * ListView over hygiene tasks that are not done.
     */
    @FXML
    private ListView<CheckBox> hygieneNotDoneTasksListView;

    /**
     * ListView over hygiene tasks that are done.
     */
    @FXML
    private ListView<CheckBox> hygieneDoneTasksListView;

    /**
     * ListView over social and activity tasks that are
     * not done.
     */
    @FXML
    private ListView<CheckBox> socialNotDoneTasksListView;

    /**
     * ListView over social and activity tasks that are done.
     */
    @FXML
    private ListView<CheckBox> socialDoneTasksListView;

    /**
     * Displays the users full name.
     */
    @FXML
    private Label userLabel;

    /**
     * Displays profilepicture of the user.
     */
    @FXML
    private ImageView userPic;

    /**
     * HashMap that holds TodoTasks and the "Converted" version of CheckBox as key
     * for the careplan nutrition.
     */
    private ObservableList<CheckBox> nutritionTasks = FXCollections.observableArrayList();

    /**
     * HashMap that holds TodoTasks and the "Converted" version of CheckBox as key
     * for the careplan personal hygiene.
     */
    private ObservableList<CheckBox> hygieneTasks = FXCollections.observableArrayList();

    /**
     * HashMap that holds TodoTasks and the "Converted" version of CheckBox as key
     * for the careplan social and activity.
     */
    private ObservableList<CheckBox> socialTasks = FXCollections.observableArrayList();

    /**
     * HashMap that holds TodoTasks and the "Converted" version of CheckBox as key
     * for the careplan medication.
     */
    private ObservableList<TodoTask> medicationTasks = FXCollections.observableArrayList();

    /**
     * Displays the patients name that this careplan belongs to.
     */
    @FXML
    private Label patientLabel;

    /**
     * The patient who's careplan is being displayed.
     */
    private Patient patient;

    /**
     * Returns the users profile picture as image.
     *
     * @return user profile picture as image.
     */
    private Image getUserPicture() {
        return this.userPic.getImage();
    }

    /**
     * Returns the users full name.
     *
     * @return user full name.
     */
    private String getUserLabel() {
        return this.userLabel.getText();
    }

    /**
     * Sets the patient name in the patient label.
     *
     * @param patientName the patients name to be displayed.
     */
    public void setPatientLabel(String patientName) {
        this.patientLabel.setText(patientName);
    }

    /**
     * Sets the users name in a label to display.
     *
     * @param usersName the usersname to be displayed.
     */
    public void setUserLabel(String usersName) {
        this.userLabel.setText(usersName);
    }

    /**
     * Formats the users profile picture.
     *
     * @param userPic picture to be formatted.
     */
    public void setUserPic(Image userPic) {
        this.userPic.setImage(userPic);
        this.userPic.setFitHeight(75.0);
        this.userPic.setFitWidth(75.0);
        Circle circle = new Circle(this.userPic.getFitHeight()/2);
        circle.setLayoutY(37.5);
        circle.setLayoutX(37.5);
        circle.setStyle("-fx-border-color: #417bc5; -fx-border-width: 2px; ");
        this.userPic.setClip(circle);
    }

    /**
     * Logs the current user off and replaces the current.
     * scene with the loginpage-scene.
     */
    @FXML
    private void logout() {
        Parent root = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().
                    getResource("Loginpage.fxml"));
            root = loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/Stylesheets/application.css").toExternalForm());
            Main.getStage().setScene(scene);
            Main.getStage().sizeToScene();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Goes back to the previous scene, i.e loads the patientpage scene.
     */
    @FXML
    private void goBack() {
        Parent root = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("PatientPage.fxml"));
            root = loader.load();
            PatientPageController controller = loader.getController();
            controller.setUserLabel(getUserLabel());
            controller.setUserPicture(getUserPicture());
            Scene scene = new Scene(root);
            Main.getStage().setScene(scene);
            Main.getStage().getScene().getStylesheets().add(getClass()
                    .getResource("/Stylesheets/application.css").toExternalForm());
            Main.getStage().sizeToScene();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the patient from the previous scene and sets up the
     * careplan lists ready for being displayed.
     *
     * @param patient the patient who's careplans is to be displayed.
     */
    public void setCarePlan(Patient patient) {
        //Sets the patient from the previous scene.
        this.patient = patient;

        //Sets the patients careplans in observable lists.
        this.setObservableLists();

        //Sets up the ****-views, journals and other necessary information.
        this.setUpMedicationTableView();
        this.setCareplanTabPane();
        this.updateJournalList();
        this.setJournalField();
        this.setCprLabel();

        //Selects the info pane first.
        this.infoplanNutrition.getSelectionModel().select(1);
        this.infoplanHygiene.getSelectionModel().select(1);
        this.infoplanSocial.getSelectionModel().select(1);
        this.infoplanMedication.getSelectionModel().select(1);

        //Creates the buttons and corresponding actions and adds them to given listview.
        setButtonsToListView(this.nutritionTaskDoneButtons, this.nutritionDoneTasksListView);
        setButtonsToListView(this.nutritionTaskNotDoneButtons, this.nutritionNotDoneTasksListView);
        setButtonsToListView(this.hygieneTaskDoneButtons, this.hygieneDoneTasksListView);
        setButtonsToListView(this.hygieneTaskNotDoneButtons, this.hygieneNotDoneTasksListView);
        setButtonsToListView(this.socialTaskDoneButtons, this.socialDoneTasksListView);
        setButtonsToListView(this.socialTaskNotDoneButtons, this.socialNotDoneTasksListView);

        //Activates the patientinfo-link
        this.patientinfoLink.setOnMousePressed(e -> patientinfo());

        //Calls the method to fill it with lists and fires the alltaskbutton.
        this.displayTasks();
        this.allTasksButton.fire();
    }

    /**
     * Depending on which button is pressed, tasks corresponding with the
     * button is displayed in the listview.
     */
    private void displayTasks() {

        this.allTasksButton.setOnAction(e -> {
            //Makes the other buttons semi-tranparent when this is pressed.
            this.allTasksButton.getGraphic().setStyle("-fx-opacity: 1;");
            this.morningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.afternoonButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.eveningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.nightButton.getGraphic().setStyle("-fx-opacity: 0.5;");

            //Set the buttonlist value.
            this.buttonList = ButtonList.ALL;

            //Gets the active tab.
            Tab tab = this.careplanTabPane.getSelectionModel().getSelectedItem();

            if (tab.equals(this.careplanTabPane.getTabs().get(0))) {
                ObservableList<CheckBox> nutritionTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> nutritionTasksDone = FXCollections.observableArrayList();
                //Gets the nutrition careplan and adds the checkboxes in relevant listviews.
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Ernæring").getTasks()) {
                    if(todoTask.isDone()) {
                        nutritionTasksDone.add(todoTask.getCheckbox());
                    } else {
                        nutritionTasks.add(todoTask.getCheckbox());
                    }
                }

                //Add listeners to the checkboxes so they switch list when isSelected is changed.
                setCheckBoxListener(nutritionTasks);
                setCheckBoxListener(nutritionTasksDone);

                this.nutritionDoneTasksListView.setItems(nutritionTasksDone);
                this.nutritionNotDoneTasksListView.setItems(nutritionTasks);

            } else if (tab.equals(this.careplanTabPane.getTabs().get(1))) {
                ObservableList<CheckBox> hygieneTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> hygieneTasksDone = FXCollections.observableArrayList();

                //Gets the personal hygiene careplan and adds the checkboxes in relevant listviews.
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getTasks()) {
                    if(todoTask.isDone()) {
                        hygieneTasksDone.add(todoTask.getCheckbox());
                    } else {
                        hygieneTasks.add(todoTask.getCheckbox());
                    }
                }

                //Add listeners to the checkboxes so they switch list when isSelected is changed.
                setCheckBoxListener(hygieneTasks);
                setCheckBoxListener(hygieneTasksDone);
                this.hygieneDoneTasksListView.setItems(hygieneTasksDone);
                this.hygieneNotDoneTasksListView.setItems(hygieneTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(2))) {
                ObservableList<CheckBox> socialTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> socialTasksDone = FXCollections.observableArrayList();

                //Gets the social and activity careplan and adds the checkboxes in relevant listviews
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getTasks()) {
                    if(todoTask.isDone()) {
                        socialTasksDone.add(todoTask.getCheckbox());
                    } else {
                        socialTasks.add(todoTask.getCheckbox());
                    }
                }

                //Add listeners to the checkboxes so they switch list when isSelected is changed.
                setCheckBoxListener(socialTasks);
                setCheckBoxListener(socialTasksDone);
                this.socialDoneTasksListView.setItems(socialTasksDone);
                this.socialNotDoneTasksListView.setItems(socialTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(3))) {
                ObservableList<TodoTask> medicationTasks = FXCollections.observableArrayList();
                //Gets the medication careplan and adds the todotasks in the tableview.
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Legemidler").getTasks()) {
                    medicationTasks.add(todoTask);
                }
                this.medicationTableView.setItems(medicationTasks);
            }
        });

        this.morningButton.setOnAction(e -> {
            this.allTasksButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.morningButton.getGraphic().setStyle("-fx-opacity: 1;");
            this.afternoonButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.eveningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.nightButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.buttonList = ButtonList.MORNING;
            Tab tab = this.careplanTabPane.getSelectionModel().getSelectedItem();
            if (tab.equals(this.careplanTabPane.getTabs().get(0))) {
                ObservableList<CheckBox> nutritionTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> nutritionTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Ernæring").getTasksByTimeOfDay("Morgen")) {
                    if(todoTask.isDone()) {
                        nutritionTasksDone.add(todoTask.getCheckbox());
                    } else {
                        nutritionTasks.add(todoTask.getCheckbox());
                    }
                }

                //Add listeners to the checkboxes so they switch list when isSelected is changed.
                setCheckBoxListener(nutritionTasks);
                setCheckBoxListener(nutritionTasksDone);
                this.nutritionDoneTasksListView.setItems(nutritionTasksDone);
                this.nutritionNotDoneTasksListView.setItems(nutritionTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(1))) {
                ObservableList<CheckBox> hygieneTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> hygieneTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getTasksByTimeOfDay("Morgen")) {
                    if(todoTask.isDone()) {
                        hygieneTasksDone.add(todoTask.getCheckbox());
                    } else {
                        hygieneTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(hygieneTasks);
                setCheckBoxListener(hygieneTasksDone);
                this.hygieneDoneTasksListView.setItems(hygieneTasksDone);
                this.hygieneNotDoneTasksListView.setItems(hygieneTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(2))) {
                ObservableList<CheckBox> socialTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> socialTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getTasksByTimeOfDay("Morgen")) {
                    if(todoTask.isDone()) {
                        socialTasksDone.add(todoTask.getCheckbox());
                    } else {
                        socialTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(socialTasks);
                setCheckBoxListener(socialTasksDone);
                this.socialDoneTasksListView.setItems(socialTasksDone);
                this.socialNotDoneTasksListView.setItems(socialTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(3))) {
                this.allTasksButton.fire();
            }
        });

        this.afternoonButton.setOnAction(e -> {
            this.allTasksButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.morningButton.getGraphic().setStyle("-fx-opacity: 0.5");
            this.afternoonButton.getGraphic().setStyle("-fx-opacity: 1;");
            this.eveningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.nightButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.buttonList = ButtonList.AFTERNOON;
            Tab tab = this.careplanTabPane.getSelectionModel().getSelectedItem();
            if (tab.equals(this.careplanTabPane.getTabs().get(0))) {
                ObservableList<CheckBox> nutritionTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> nutritionTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Ernæring").getTasksByTimeOfDay("Ettermiddag")) {
                    if(todoTask.isDone()) {
                        nutritionTasksDone.add(todoTask.getCheckbox());
                    } else {
                        nutritionTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(nutritionTasks);
                setCheckBoxListener(nutritionTasksDone);
                this.nutritionDoneTasksListView.setItems(nutritionTasksDone);
                this.nutritionNotDoneTasksListView.setItems(nutritionTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(1))) {
                ObservableList<CheckBox> hygieneTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> hygieneTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getTasksByTimeOfDay("Ettermiddag")) {
                    if(todoTask.isDone()) {
                        hygieneTasksDone.add(todoTask.getCheckbox());
                    } else {
                        hygieneTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(hygieneTasks);
                setCheckBoxListener(hygieneTasksDone);
                this.hygieneDoneTasksListView.setItems(hygieneTasksDone);
                this.hygieneNotDoneTasksListView.setItems(hygieneTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(2))) {
                ObservableList<CheckBox> socialTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> socialTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getTasksByTimeOfDay("Ettermiddag")) {
                    if(todoTask.isDone()) {
                        socialTasksDone.add(todoTask.getCheckbox());
                    } else {
                        socialTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(socialTasks);
                setCheckBoxListener(socialTasksDone);
                this.socialDoneTasksListView.setItems(socialTasksDone);
                this.socialNotDoneTasksListView.setItems(socialTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(3))) {
                this.allTasksButton.fire();
            }
        });

        this.eveningButton.setOnAction(e -> {
            this.allTasksButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.morningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.afternoonButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.eveningButton.getGraphic().setStyle("-fx-opacity: 1;");
            this.nightButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.buttonList = ButtonList.EVENING;
            Tab tab = this.careplanTabPane.getSelectionModel().getSelectedItem();
            if (tab.equals(this.careplanTabPane.getTabs().get(0))) {
                ObservableList<CheckBox> nutritionTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> nutritionTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Ernæring").getTasksByTimeOfDay("Kveld")) {
                    if(todoTask.isDone()) {
                        nutritionTasksDone.add(todoTask.getCheckbox());
                    } else {
                        nutritionTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(nutritionTasks);
                setCheckBoxListener(nutritionTasksDone);
                this.nutritionDoneTasksListView.setItems(nutritionTasksDone);
                this.nutritionNotDoneTasksListView.setItems(nutritionTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(1))) {
                ObservableList<CheckBox> hygieneTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> hygieneTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getTasksByTimeOfDay("Kveld")) {
                    if(todoTask.isDone()) {
                        hygieneTasksDone.add(todoTask.getCheckbox());
                    } else {
                        hygieneTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(hygieneTasks);
                setCheckBoxListener(hygieneTasksDone);
                this.hygieneDoneTasksListView.setItems(hygieneTasksDone);
                this.hygieneNotDoneTasksListView.setItems(hygieneTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(2))) {
                ObservableList<CheckBox> socialTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> socialTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getTasksByTimeOfDay("Kveld")) {
                    if(todoTask.isDone()) {
                        socialTasksDone.add(todoTask.getCheckbox());
                    } else {
                        socialTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(socialTasks);
                setCheckBoxListener(socialTasksDone);
                this.socialDoneTasksListView.setItems(socialTasksDone);
                this.socialNotDoneTasksListView.setItems(socialTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(3))) {
                this.allTasksButton.fire();
            }
        });

        this.nightButton.setOnAction(e -> {
            this.allTasksButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.morningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.afternoonButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.eveningButton.getGraphic().setStyle("-fx-opacity: 0.5;");
            this.nightButton.getGraphic().setStyle("-fx-opacity: 1;");
            Tab tab = this.careplanTabPane.getSelectionModel().getSelectedItem();
            if (tab.equals(this.careplanTabPane.getTabs().get(0))) {
                ObservableList<CheckBox> nutritionTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> nutritionTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Ernæring").getTasksByTimeOfDay("Natt")) {
                    if(todoTask.isDone()) {
                        nutritionTasksDone.add(todoTask.getCheckbox());
                    } else {
                        nutritionTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(nutritionTasks);
                setCheckBoxListener(nutritionTasksDone);
                this.nutritionDoneTasksListView.setItems(nutritionTasksDone);
                this.nutritionNotDoneTasksListView.setItems(nutritionTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(1))) {
                ObservableList<CheckBox> hygieneTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> hygieneTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getTasksByTimeOfDay("Natt")) {
                    if(todoTask.isDone()) {
                        hygieneTasksDone.add(todoTask.getCheckbox());
                    } else {
                        hygieneTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(hygieneTasks);
                setCheckBoxListener(hygieneTasksDone);
                this.hygieneDoneTasksListView.setItems(hygieneTasksDone);
                this.hygieneNotDoneTasksListView.setItems(hygieneTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(2))) {
                ObservableList<CheckBox> socialTasks = FXCollections.observableArrayList();
                ObservableList<CheckBox> socialTasksDone = FXCollections.observableArrayList();
                for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getTasksByTimeOfDay("Natt")) {
                    if(todoTask.isDone()) {
                        socialTasksDone.add(todoTask.getCheckbox());
                    } else {
                        socialTasks.add(todoTask.getCheckbox());
                    }
                }
                setCheckBoxListener(socialTasks);
                setCheckBoxListener(socialTasksDone);
                this.socialDoneTasksListView.setItems(socialTasksDone);
                this.socialNotDoneTasksListView.setItems(socialTasks);
            } else if (tab.equals(this.careplanTabPane.getTabs().get(3))) {
                this.allTasksButton.fire();
            }
        });
    }

    /**
     * Loads the the patientinfo window for the patient connected to
     * the careplans.
     */
    private void patientinfo () {
        FXMLLoader loader = new FXMLLoader(getClass().
                getResource("PatientInfo.fxml"));
        try {
            Parent root = loader.load();
            PatientInfoController controller = loader.getController();
            controller.setPatientInfoScene(patient);
            controller.setUsersName(getUserLabel());
            controller.setUsersPicture(getUserPicture());

            Stage stage = new Stage();
            stage.setTitle("Brukerinfo");
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.getScene().getStylesheets().add(getClass().
                    getResource("/Stylesheets/pasientinfo.css").toExternalForm());
            stage.show();
            Main.getStage().setOnCloseRequest(windowEvent -> stage.close());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the toolbar that exist on top of every ListView with controls
     * to add and remove tasks.
     */
    private void setButtonsToListView(HBox toolbar, ListView listView) {
        Button addTask = new Button();
        Image addImage = new Image(getClass().getResource("/Icons/add.png").toExternalForm());
        ImageView addIcon = new ImageView(addImage);
        addIcon.setPreserveRatio(true);
        addIcon.setFitWidth(25);
        addTask.setGraphic(addIcon);
        addTask.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            Careplan careplan = null;
            if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                careplan = patient.findCareplanByTypeOfCare("Ernæring");
            } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                careplan = patient.findCareplanByTypeOfCare("Personlig hygiene");
            } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                careplan = patient.findCareplanByTypeOfCare("Sosial og aktivitet");
            }
            TaskDetailsDialog taskDetailsDialog = new TaskDetailsDialog();
            Optional<TodoTask> result = taskDetailsDialog.showAndWait();
            if (result.isPresent()) {

                TodoTask todoTask = result.get();
                careplan.getTasks().add(todoTask);
                setObservableLists();
                findLastUsedButton().fire();
            }
        });

        Button changeTask = new Button();
        Image changeImage = new Image(getClass().getResource("/Icons/edit.png").toExternalForm());
        ImageView changeIcon = new ImageView(changeImage);
        changeIcon.setPreserveRatio(true);
        changeIcon.setFitWidth(25);
        changeTask.setGraphic(changeIcon);
        changeTask.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            CheckBox checkBox = (CheckBox) listView.getSelectionModel().getSelectedItem();
            if(checkBox == null) {

            } else {
                if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                    TaskDetailsDialog taskDetailsDialog = new TaskDetailsDialog(this.patient.findCareplanByTypeOfCare("Ernæring").findTaskByCheckBox(checkBox));
                    taskDetailsDialog.showAndWait();
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                    TaskDetailsDialog taskDetailsDialog = new TaskDetailsDialog(this.patient.findCareplanByTypeOfCare("Peronlig hygiene").findTaskByCheckBox(checkBox));
                    taskDetailsDialog.showAndWait();
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                    TaskDetailsDialog taskDetailsDialog = new TaskDetailsDialog(this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").findTaskByCheckBox(checkBox));
                    taskDetailsDialog.showAndWait();
                    findLastUsedButton().fire();
                }
            }

        });

        Button removeTask = new Button();
        Image removeImage = new Image(getClass().getResource("/Icons/remove.png").toExternalForm());
        ImageView removeIcon = new ImageView(removeImage);
        removeIcon.setPreserveRatio(true);
        removeIcon.setFitWidth(25);
        removeTask.setGraphic(removeIcon);
        removeTask.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            CheckBox checkBox = (CheckBox) listView.getSelectionModel().getSelectedItem();
            if(checkBox == null) {

            } else {
                if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                    this.patient.findCareplanByTypeOfCare("Ernæring").removeTask(this.patient.findCareplanByTypeOfCare("Ernæring").findTaskByCheckBox(checkBox));
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                    this.patient.findCareplanByTypeOfCare("Personlig hygiene").removeTask(this.patient.findCareplanByTypeOfCare("Personlig hygiene").findTaskByCheckBox(checkBox));
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                    this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").removeTask(this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").findTaskByCheckBox(checkBox));
                    findLastUsedButton().fire();
                }
            }
        });

        Button moveTaskUp = new Button();
        Image moveTaskUpImage = new Image(getClass().getResource("/Icons/up.png").toExternalForm());
        ImageView moveTaskUpIcon = new ImageView(moveTaskUpImage);
        moveTaskUpIcon.setPreserveRatio(true);
        moveTaskUpIcon.setFitWidth(25);
        moveTaskUp.setGraphic(moveTaskUpIcon);
        moveTaskUp.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            CheckBox checkBox = (CheckBox) listView.getSelectionModel().getSelectedItem();
            if(checkBox == null) {

            } else {
                if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Ernæring").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Ernæring").changeTaskPositionUp(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Personlig hygiene").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Personlig hygiene").changeTaskPositionUp(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").changeTaskPositionUp(todoTask);
                    findLastUsedButton().fire();
                }
            }
        });

        Button moveTaskDown = new Button();
        Image moveTaskDownImage = new Image(getClass().getResource("/Icons/down.png").toExternalForm());
        ImageView moveTaskDownIcon = new ImageView(moveTaskDownImage);
        moveTaskDownIcon.setPreserveRatio(true);
        moveTaskDownIcon.setFitWidth(25);
        moveTaskDown.setGraphic(moveTaskDownIcon);
        moveTaskDown.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            CheckBox checkBox = (CheckBox) listView.getSelectionModel().getSelectedItem();
            if(checkBox == null) {

            } else {
                if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Ernæring").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Ernæring").changeTaskPositionDown(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Personlig hygiene").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Personlig hygiene").changeTaskPositionDown(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").changeTaskPositionDown(todoTask);
                    findLastUsedButton().fire();
                }
            }
        });

        Button moveTaskToBottom = new Button();
        Image moveTaskToBottomImage = new Image(getClass().getResource("/Icons/bottom.png").toExternalForm());
        ImageView moveTaskToBottomIcon = new ImageView(moveTaskToBottomImage);
        moveTaskToBottomIcon.setPreserveRatio(true);
        moveTaskToBottomIcon.setFitWidth(25);
        moveTaskToBottom.setGraphic(moveTaskToBottomIcon);
        moveTaskToBottom.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            CheckBox checkBox = (CheckBox) listView.getSelectionModel().getSelectedItem();
            if(checkBox == null) {

            } else {
                if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Ernæring").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Ernæring").changeTaskPostitionBottom(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Personlig hygiene").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Personlig hygiene").changeTaskPostitionBottom(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").changeTaskPostitionBottom(todoTask);
                    findLastUsedButton().fire();
                }
            }
        });

        Button moveTaskToTop = new Button();
        Image moveTaskToTopImage = new Image(getClass().getResource("/Icons/top.png").toExternalForm());
        ImageView moveTaskToTopIcon = new ImageView(moveTaskToTopImage);
        moveTaskToTopIcon.setPreserveRatio(true);
        moveTaskToTopIcon.setFitWidth(25);
        moveTaskToTop.setGraphic(moveTaskToTopIcon);
        moveTaskToTop.setOnAction(e -> {
            Tab tabSelected = this.careplanTabPane.getSelectionModel().getSelectedItem();
            CheckBox checkBox = (CheckBox) listView.getSelectionModel().getSelectedItem();
            if(checkBox == null) {

            } else {
                if(tabSelected.equals(this.careplanTabPane.getTabs().get(0))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Ernæring").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Ernæring").changeTaskPostitionTop(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(1))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Personlig hygiene").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Personlig hygiene").changeTaskPostitionTop(todoTask);
                    findLastUsedButton().fire();
                } else if(tabSelected.equals(this.careplanTabPane.getTabs().get(2))) {
                    TodoTask todoTask = this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").findTaskByCheckBox(checkBox);
                    this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").changeTaskPostitionTop(todoTask);
                    findLastUsedButton().fire();
                }
            }
        });

        toolbar.setStyle("-fx-border-color: white; -fx-background-color:  #8eb9f0");
        toolbar.getChildren().addAll(removeTask, changeTask, addTask, moveTaskUp, moveTaskDown, moveTaskToTop, moveTaskToBottom);
    }

    /**
     * Retrieves the careplans and places them in observable lists.
     */
    private void setObservableLists() {
        for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Ernæring").getTasks()) {
            this.nutritionTasks.add(todoTask.getCheckbox());
        }
        for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getTasks()) {
            this.hygieneTasks.add(todoTask.getCheckbox());
        }
        for(TodoTask todoTask : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getTasks()) {
            this.socialTasks.add(todoTask.getCheckbox());
        }
        this.medicationTasks.addAll(this.patient.findCareplanByTypeOfCare("Legemidler").getTasks());
    }

    /**
     * Finds the last used "time of day" button.
     *
     * @return last used "time of day" button.
     */
    private Button findLastUsedButton() {
        Button buttonLastUsed = null;

        if(buttonList == ButtonList.ALL) {
            buttonLastUsed = this.allTasksButton;
        } else if(buttonList == ButtonList.MORNING) {
            buttonLastUsed = this.morningButton;
        } else if (buttonList == ButtonList.AFTERNOON) {
            buttonLastUsed = this.afternoonButton;
        } else if (buttonList == ButtonList.EVENING) {
            buttonLastUsed = this.eveningButton;
        } else if (buttonList == ButtonList.NIGHT) {
            buttonLastUsed = this.nightButton;
        }
        return buttonLastUsed;
    }

    /**
     * Adds listener to the careplan tabs so the last used "time of day"
     * button will display.
     */
    private void setCareplanTabPane() {
        for (Tab tab : this.careplanTabPane.getTabs()) {
            tab.selectedProperty().addListener(e -> findLastUsedButton().fire());
        }
    }

    /**
     * Displays the journalentries.
     */
    private void updateJournalList() {
        this.nutritionJournalList.setFocusTraversable(false);
        this.nutritionJournalList.setMouseTransparent(true);
        this.hygieneJournalList.setFocusTraversable(false);
        this.hygieneJournalList.setMouseTransparent(true);
        this.socialJournalList.setFocusTraversable(false);
        this.socialJournalList.setMouseTransparent(true);
        this.medicationJournalList.setFocusTraversable(false);
        this.medicationJournalList.setMouseTransparent(true);

        String nutritionJournalListAsString = "";
        for (JournalEntry entry : this.patient.findCareplanByTypeOfCare("Ernæring").getJournal()) {
            nutritionJournalListAsString += entry.getEntryTime() +" \t" + entry.getUsersName() + " \n" + entry.getJournalEntry() + " \n";
        }
        this.nutritionJournalList.setText(nutritionJournalListAsString);

        String hygieneJournalListAsString = "";
        for (JournalEntry entry : this.patient.findCareplanByTypeOfCare("Personlig hygiene").getJournal()) {
            hygieneJournalListAsString += entry.getEntryTime() +" \t" + entry.getUsersName() + " \n" + entry.getJournalEntry() + " \n";
        }
        this.hygieneJournalList.setText(hygieneJournalListAsString);

        String socialJournalListAsString = "";
        for (JournalEntry entry : this.patient.findCareplanByTypeOfCare("Sosial og aktivitet").getJournal()) {
            socialJournalListAsString += entry.getEntryTime() +" \t" + entry.getUsersName() + " \n" + entry.getJournalEntry() + " \n";
        }
        this.socialJournalList.setText(socialJournalListAsString);

        String medicationJournalListAsString = "";
        for (JournalEntry entry : this.patient.findCareplanByTypeOfCare("Legemidler").getJournal()) {
            medicationJournalListAsString += entry.getEntryTime() +" \t" + entry.getUsersName() + " \n" + entry.getJournalEntry() + " \n";
        }
        this.medicationJournalList.setText(medicationJournalListAsString);
    }

    /**
     * Adds key event to the journal fields so a new journal entry will
     * be created in the appropriate careplan.
     */
    private void setJournalField() {
        this.nutritionJournalField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent key) {
                if (key.getCode().equals(KeyCode.ENTER)) {
                    patient.findCareplanByTypeOfCare("Ernæring").addJournalEntry(new JournalEntry(nutritionJournalField.getText(), getUserLabel()));
                    nutritionJournalField.setText("");
                    updateJournalList();
                }
            }
        });

        this.hygieneJournalField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent key) {
                if (key.getCode().equals(KeyCode.ENTER)) {
                    patient.findCareplanByTypeOfCare("Personlig hygiene").addJournalEntry(new JournalEntry(hygieneJournalField.getText(), getUserLabel()));
                    hygieneJournalField.setText("");
                    updateJournalList();
                }
            }
        });

        this.socialJournalField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent key) {
                if (key.getCode().equals(KeyCode.ENTER)) {
                    patient.findCareplanByTypeOfCare("Sosial og aktivitet").addJournalEntry(new JournalEntry(socialJournalField.getText(), getUserLabel()));
                    socialJournalField.setText("");
                    updateJournalList();
                }
            }
        });

        this.medicationJournalField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent key) {
                if (key.getCode().equals(KeyCode.ENTER)) {
                    patient.findCareplanByTypeOfCare("Legemidler").addJournalEntry(new JournalEntry(medicationJournalField.getText(), getUserLabel()));
                    medicationJournalField.setText("");
                    updateJournalList();
                }
            }
        });
    }

    /**
     * Adds the appropriate textcolour and text to the cpr label
     * indicating whether this patient wishes to be saved or not.
     */
    private void setCprLabel() {
        if(this.patient.doCpr()) {
            this.doCprLabel.setText("HLR+");
            this.doCprLabel.setStyle("-fx-text-fill: #2b9f2b");
        } else {
            this.doCprLabel.setText("HLR-");
            this.doCprLabel.setStyle("-fx-text-fill: red");
        }
    }

    /**
     * Adds listeners to the checkboxes so the listview will update
     * when a checkbox is selected.
     *
     * @param checkBoxList the list of checkboxes.
     */
    private void setCheckBoxListener(List<CheckBox> checkBoxList) {
        for(CheckBox checkBox : checkBoxList) {
            checkBox.selectedProperty().addListener(e -> displayTasks());
        }
    }

    /**
     * Sets the columns in the medication tableview with appropritate values.
     * Makes it only possible to be at the "All Tasks" buttons when the
     * medication tabe pane is selected.
     */
    private void setUpMedicationTableView() {
        medicationListColumn.setCellValueFactory(new PropertyValueFactory<>("checkbox"));

        medicationTimeColumn.setCellValueFactory(new PropertyValueFactory<>("timeOfDay"));
        this.medicationTableView.setPlaceholder(new Label(""));
        this.medicationTableView.setItems(this.medicationTasks);

        this.careplanTabPane.getTabs().get(3).selectedProperty().addListener(e -> {
            if(this.careplanTabPane.getTabs().get(3).isSelected()) {
                this.allTasksButton.fire();
            }
        });
    }
}
