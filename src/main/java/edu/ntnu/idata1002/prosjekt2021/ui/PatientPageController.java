package edu.ntnu.idata1002.prosjekt2021.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import edu.ntnu.idata1002.prosjekt2021.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 * Controller for the patientpage scene.
 */
public class PatientPageController {

  /**
   * Section list available for this user.
   */
  private SectionList sectionList;

  /**
   * ArrayList over patients in this scene.
   */
  private ArrayList<Patient> patients = new ArrayList<>();

  /**
   * Label with the users name.
   */
  @FXML
  private Label userLabel;

  /**
   * Profile picture of the user.
   */
  @FXML
  private ImageView userPic;

  /**
   * Switch between departments.
   */
  @FXML
  private ComboBox<String> sectionMenu;

  /**
   * The users menu.
   */
  @FXML
  private MenuButton userMenu;

  /**
   * The pane surrounding the patient list.
   */
  @FXML
  private VBox patientContentPane;

  /**
   * Searchbox for searching after patients.
   */
  @FXML
  private TextField searchBox;

  /**
   * Visual list over patients.
   */
  @FXML
  private GridPane patientListPane;

  public void initialize() {
    this.sectionList = Model.getInstanceOfSectionList();
    setSectionMenu();
    this.patients.addAll(sectionList.findSectionBySectionName
            (sectionMenu.getSelectionModel().getSelectedItem()).getPatients());
    setPatientListPane();

  }

  /**
   * Gets the users profile picture and molds it into a circle.
   *
   * @param image the users image.
   */
  public void setUserPicture(Image image) {
    userPic.setImage(image);
    userPic.setFitHeight(75.0);
    userPic.setFitWidth(75.0);
    Circle circle = new Circle(userPic.getFitHeight()/2);
    circle.setLayoutY(37.5);
    circle.setLayoutX(37.5);
    userPic.setClip(circle);

  }

  /**
   * Returns the users profile picture.
   *
   * @return users profile picture.
   */
  private Image getUserPic() {
    return this.userPic.getImage();
  }

  /**
   * Gets the users full name and adds it to a label.
   *
   * @param user the users full name.
   */
  public void setUserLabel(String user) {
    userLabel.setText(user);
  }

  /**
   * Returns the users full name from the user label.
   *
   * @return the users full name.
   */
  private String getUserLabel() {
    return this.userLabel.getText();
  }

  /**
   * Logs the current user off and replaces the current
   * scene with the loginpage-scene.
   */
  @FXML
  private void logout() {
    Parent root = null;
    try {
      FXMLLoader loader = new FXMLLoader(getClass().
              getResource("Loginpage.fxml"));
      root = loader.load();
      Scene scene = new Scene(root);
      scene.getStylesheets().add(getClass().getResource("/Stylesheets/application.css").toExternalForm());
      Main.getStage().setScene(scene);
      Main.getStage().sizeToScene();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Adds the section available in sectionlist to a combobox.
   */
  private void setSectionMenu() {
    ArrayList<String> sectionNames = new ArrayList<>();
    for(Section section : Model.getInstanceOfSectionList().getSections()) {
      sectionNames.add(section.getSectionName());
    }
    ObservableList<String> sections = FXCollections.observableArrayList
            (sectionNames);
    sectionMenu.setItems(sections);
    sectionMenu.getSelectionModel().selectFirst();
    sectionMenu.setOnAction(e -> updatePatientList());
    setPatientListPane();
    //sectionMenu.setCellFactory(sectionList., );
    /*sectionMenu.setConverter(new StringConverter<Section>() {

      @Override
      public String toString(Section object) {
        return object.getSectionName();
      }

      @Override
      public Section fromString(String string) {
        return departmentMenu.getItems().stream().filter(ap ->
            ap.getSectionName().equals(string)).findFirst().orElse(null);
      }
    });
     */
  }

  /**
   * Iterates over the patient list and places each patient in a grid with image of the
   * patient, name, address, city, postalcode and buttons for patientinfo and careplan with
   * listeners.
   */
  private void setPatientListPane() {
    int rowIndex = 0;
    int coloumnIndex = 0;
    for(Patient patient : this.patients) {
      HBox patientGrid = new HBox();
      patientGrid.setAlignment(Pos.CENTER_LEFT);
      patientGrid.prefHeight(100);
      patientGrid.prefWidth(200);
      patientGrid.setSpacing(18);
      patientGrid.setPadding(new Insets(10,10,10,10));
      patientGrid.setStyle("-fx-background-color: #f7f9fbff;");

      String socialSecurity = patient.getSocialSecurityNumber();
      Button carePlan = setPatientGridCarePlanButton();
      carePlan.setOnAction(e -> carePlan(patient));
      Button patientInfoButton = setPatientInfoButton();
      patientInfoButton.setOnAction(e -> patientInfo(patient));

      patientGrid.getChildren().addAll(setPatientGridImage(patient),
              setPatientGridLabels(patient),patientInfoButton, carePlan);

      this.patientListPane.add(patientGrid, coloumnIndex, rowIndex);
      if(coloumnIndex == 0) {
        coloumnIndex = 1;
      } else if (coloumnIndex == 1) {
        coloumnIndex = 0;
        rowIndex++;
      }
    }
  }

  /**
   * Creates an imageview from the patients image and sets correct format.
   *
   * @param patient patient who's image is to be formatted.
   * @return the imageview of the patients image.
   */
  private ImageView setPatientGridImage(Patient patient) {
    ImageView picture = new ImageView(patient.getPicture());
    picture.setFitWidth(80.0);
    picture.setFitHeight(80.0);

    return picture;
  }

  /**
   * Sets the labels containing name and address of the given patient, and adds
   * them to a VBox.
   *
   * @param patient patient who's name and address is being set to label.
   * @return VBox containing labels with the patients name and address.
   */
  private VBox setPatientGridLabels(Patient patient) {
    Label cprLabel = new Label();
    if(patient.doCpr()) {
      cprLabel.setText("HLR+");
      cprLabel.setStyle("-fx-text-fill: #2b9f2b");
    } else {
      cprLabel.setText("HLR-");
      cprLabel.setStyle("-fx-text-fill: red");
    }
    Label patientName = new Label(patient.getFullName());
    Label patientAddress = new Label(patient.getAddress());
    Label patientPostalArea = new Label(patient.getPostalCode() + " " +patient.getCity());
    VBox patientLabels = new VBox();
    patientLabels.getChildren().addAll(cprLabel, patientName, patientAddress, patientPostalArea);
    patientLabels.setAlignment(Pos.CENTER_LEFT);
    patientLabels.setPrefWidth(155);

    return patientLabels;
  }

  /**
   * Sets the patientinfo button.
   *
   * @return patientinfo button.
   */
  private Button setPatientInfoButton() {
    ImageView patienInfoIcon = new ImageView(new Image(getClass().getResource("/Icons/pasientinfo.png").toExternalForm()));
    patienInfoIcon.setFitHeight(50);
    patienInfoIcon.setFitWidth(50);
    Button patientInfo = new Button("Pasientinfo", patienInfoIcon);
    patientInfo.setAlignment(Pos.BOTTOM_CENTER);
    patientInfo.setContentDisplay(ContentDisplay.TOP);
    dropShadowEffectHoveringCursor(patientInfo);

    return patientInfo;
  }

  /**
   * Sets the careplan button.
   *
   * @return careplan button.
   */
  private Button setPatientGridCarePlanButton() {
    ImageView carePlanIcon = new ImageView(new Image(getClass().getResource("/Icons/journal.png").toExternalForm()));
    carePlanIcon.setFitWidth(50);
    carePlanIcon.setFitHeight(50);
    Button carePlan = new Button("Tiltaksplan", carePlanIcon);
    carePlan.setAlignment(Pos.BOTTOM_CENTER);
    carePlan.setContentDisplay(ContentDisplay.TOP);
    dropShadowEffectHoveringCursor(carePlan);

    return carePlan;
  }

  /**
   * Adds shadow effects on a given button.
   *
   * @param button the button to set shadow effects on.
   */
  private void dropShadowEffectHoveringCursor(Button button) {
    DropShadow shadow = new DropShadow(5.0, Color.web("324a6872"));

    //Adding the shadow when the mouse cursor is on
    button.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> button.setEffect(shadow));

    //Removing the shadow when the mouse cursor is off
    button.addEventHandler(MouseEvent.MOUSE_EXITED, e -> button.setEffect(null));


    InnerShadow innerShadow = new InnerShadow(50.0, Color.web("324a6872"));

    //Darkening when button is pressed
    button.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> button.setEffect(innerShadow));

    //Removing inner shadow when no longer pressed
    button.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent e) {
        button.setEffect(null);
        button.setEffect(shadow);
      }
    });
  }

  /**
   * Loads the careplan belonging to the patient given.
   *
   * @param patient the patient who's careplan is to be loaded
   */
  private void carePlan ( Patient patient) {
    Parent root = null;
    try {
      FXMLLoader loader = new FXMLLoader(getClass().
              getResource("CarePlan.fxml"));
      root = loader.load();
      CarePlanController controller = loader.getController();
      controller.setPatientLabel(getPatientName(patient));
      controller.setUserPic(getUserPic());
      controller.setUserLabel(getUserLabel());
      controller.setCarePlan(patient);
      Scene scene = new Scene(root);
      Main.getStage().setScene(scene);
      Main.getStage().getScene().getStylesheets().add(getClass()
              .getResource("/Stylesheets/application.css").toExternalForm());
      Main.getStage().sizeToScene();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Opens a new window with the patientinfo scene of the given patient.
   */
  private void patientInfo (Patient patient) {
    FXMLLoader loader = new FXMLLoader(getClass().
            getResource("PatientInfo.fxml"));
    try {
      Parent root = loader.load();
      PatientInfoController controller = loader.getController();
      controller.setPatientInfoScene(patient);
      controller.setUsersName(getUserLabel());
      controller.setUsersPicture(getUserPic());

      Stage stage = new Stage();
      stage.setTitle("Brukerinfo");
      controller.setStage(stage);
      Scene scene = new Scene(root);
      stage.setScene(scene);
      stage.getScene().getStylesheets().add(getClass().
              getResource("/Stylesheets/pasientinfo.css").toExternalForm());
      stage.show();
      Main.getStage().setOnCloseRequest(windowEvent -> stage.close());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Iterates over the patients list to find the name of a patient with the given
   * social security number.
   *
   * @param patient the patient who's name is to be found.
   * @return the name of the patient
   */
  private String getPatientName(Patient patient) {
    boolean notFound = true;
    String patientName = null;

    Iterator<Patient> iterator = patients.iterator();
    while(notFound && iterator.hasNext()) {
      Patient patients = iterator.next();
      if(patients.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber())) {
        patientName = patient.getFullName();
        notFound = false;
      }
    }

    return patientName;
  }

  /**
   * Loads and updates the patient with the other section selected.
   * Note it is not been tested.
   *
   * @return list with the patients from the other section.
   */
  private List<Patient> updatePatientList() {
    this.patients.clear();
    this.patients.addAll(sectionList.findSectionBySectionName
            (sectionMenu.getSelectionModel().getSelectedItem()).getPatients());

    return this.patients;
  }
}

