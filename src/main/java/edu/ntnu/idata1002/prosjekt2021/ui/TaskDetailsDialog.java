package edu.ntnu.idata1002.prosjekt2021.ui;


import edu.ntnu.idata1002.prosjekt2021.model.TodoTask;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

/**
 * Opens a dialog window for user input to create or edit tasks.
 */
public class TaskDetailsDialog extends Dialog<TodoTask> {

    /**
     * Modes of dialogpane.
     */
    public enum Mode {
        NEW, EDIT
    }

    /**
     * This dialog panes mode.
     */
    private final Mode mode;

    /**
     * TodoTask to be edited, if any.
     */
    private TodoTask task = null;

    /**
     * Opens dialog window for adding a new task.
     */
    public TaskDetailsDialog() {
        super();
        this.mode = Mode.NEW;
        createContent();

    }

    /**
     * Opens dialog window to edit existing task.
     * @param task the task to be edited.
     */
    public TaskDetailsDialog(TodoTask task) {
        super();
        this.mode = Mode.EDIT;
        this.task = task;
        createContent();
    }

    /**
     * Creates the content of the task details dialog.
     */
    private void createContent() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField description = new TextField();
        description.setPromptText("Beskrivelse");

        ComboBox deadLine = new ComboBox();
        deadLine.getItems().addAll("Morgen", "Ettermiddag", "Kveld", "Natt");
        deadLine.setPromptText("Tidsfrist");


        grid.add(new Label("Beskrivelse: "), 0, 0);
        grid.add(description, 1, 0);
        grid.add(new Label("Tidsfrist: "), 0, 1);
        grid.add(deadLine, 1, 1);

        getDialogPane().setContent(grid);
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        getDialogPane().getStylesheets().add(getClass().getResource("/Stylesheets/dialogs.css").toExternalForm());

        final Button okButton = (Button) getDialogPane().lookupButton(ButtonType.OK);
        okButton.addEventFilter(ActionEvent.ACTION, event -> {
            //Continue the dialog if one of the specified conditions happen.
            if (description.getText().isBlank() || deadLine.getSelectionModel().getSelectedItem()==null) {
                event.consume();
            }
        });

        if ((mode == Mode.EDIT)) {
            //Custom setup of dialog for EDIT mode
            setTitle("Endre gjøremål");
            getDialogPane().setHeaderText("Endre gjøremål");
            description.setText(this.task.getCheckbox().getText());
            for(Object timeOfDay : deadLine.getItems()) {
                if (task.getTimeOfDay().equals(timeOfDay)) {
                    deadLine.getSelectionModel().select(timeOfDay);
                }
            }
        } else if (mode == Mode.NEW) {
            //Custom setup of dialog for NEW mode
            setTitle("Legg til nytt gjøremål");
            getDialogPane().setHeaderText("Legg til nytt gjøremål");
        }

        //Convert result into TodoTask when OK button is pressed.
        setResultConverter((ButtonType button) -> {
            TodoTask result = null;
            if (button == ButtonType.OK) {
                if (mode == Mode.NEW) {
                    result = new TodoTask(description.getText(), (String) deadLine.getSelectionModel().getSelectedItem());
                } else if (mode == Mode.EDIT) {
                    this.task.setDescription(description.getText());
                    if(deadLine.getSelectionModel().getSelectedItem() != null) {
                        this.task.setTimeOfDay((String) deadLine.getSelectionModel().getSelectedItem());
                        result = this.task;
                    }
                }
            } else if (button == ButtonType.CANCEL) {
                close();
            }
            return result;
        });
    }
}
