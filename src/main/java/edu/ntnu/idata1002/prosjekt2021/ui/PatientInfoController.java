package edu.ntnu.idata1002.prosjekt2021.ui;

import edu.ntnu.idata1002.prosjekt2021.model.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * Controller for the patientinfo scene.
 */
public class PatientInfoController {

    /**
     * Link to the careplan for patient who is showing.
     */
    @FXML
    private Label careplanLink;

    /**
     * Displays the patients full name.
     */
    @FXML
    private Label patientName;

    /**
     * Displays a picture of the patient
     */
    @FXML
    private ImageView patientImage;

    /**
     * Displays a short summary of the patient.
     */
    @FXML
    private TextArea summaryTextArea;

    /**
     * Displays additional information about the patient.
     */
    @FXML
    private TextArea additionalInformationTextArea;

    /**
     * Displays the patients birthdate.
     */
    @FXML
    private Label socialSecurity;

    /**
     * Displays the patients address.
     */
    @FXML
    private Label address;

    /**
     * Displays the name of the patients emergency contact.
     */
    @FXML
    private Label emergencyContactName;

    /**
     * Displays the name of the patients general practitioner
     */
    @FXML
    private Label generalPractitionerName;

    /**
     * Displays the phonenumber of the patients general practitioner.
     */
    @FXML
    private Label generalPractitionerPhonenumber;

    /**
     * Displays the phonenumber for the patient's emergency contact.
     */
    @FXML
    private Label emergencyContactPhonenumber;

    /**
     * Displays the patient's diagnosis.
     */
    @FXML
    private TextArea diagnosisTextArea;

    /**
     * The stage of this scene.
     */
    private Stage stage;

    /**
     * The patient whom's info is being displayed.
     */
    private Patient patient;

    /**
     * The user's full name.
     */
    private String usersName;

    /**
     * The profile picture of the user.
     */
    private Image usersPicture;

    /**
     * Sets information about the patient in labels and
     * text area to be displayed for the user.
     *
     * @param patient the patients who's information is being set.
     */
    public void setPatientInfoScene(Patient patient) {
        this.patient = patient;
        this.patientName.setText(patient.getFullName());
        this.patientImage.setImage(patient.getPicture());
        this.address.setText(patient.getAddress() + ", "
                +patient.getPostalCode() + " "
                + patient.getCity());
        this.emergencyContactName.setText(patient.getEmergencyContact().getFullName());
        this.emergencyContactPhonenumber.setText(patient.getEmergencyContact().getPhonenumber());
        this.generalPractitionerName.setText(patient.getGeneralPractitioner().getFullName());
        this.generalPractitionerPhonenumber.setText(patient.getGeneralPractitioner().getPhonenumber());
        this.socialSecurity.setText(patient.getSocialSecurityNumber());
        this.summaryTextArea.setText(patient.getSummary());
        this.additionalInformationTextArea.setText(patient.getAdditionalInformation());
        this.diagnosisTextArea.setText(patient.getDiagnosis());
        this.careplanLink.setOnMousePressed(mouseEvent -> carePlan(this.stage));
    }

    /**
     * Loads the careplan. Closes the given stage.
     *
     * @param stage the stage to be closed when the careplan is opened.
     */
    @FXML
    private void carePlan (Stage stage) {
        stage.close();
        Parent root = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass()
                    .getResource("CarePlan.fxml"));
            root = loader.load();
            CarePlanController controller = loader.getController();
            controller.setPatientLabel(patient.getFullName());
            controller.setUserPic(getUsersPicture());
            controller.setUserLabel(getUsersName());
            controller.setCarePlan(patient);
            Scene scene = new Scene(root);
            this.stage.close();
            Main.getStage().setScene(scene);
            Main.getStage().getScene().getStylesheets().add(getClass().
                    getResource("/Stylesheets/application.css").toExternalForm());
            Main.getStage().sizeToScene();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the users full name.
     *
     * @param usersName full name of the user set.
     */
    public void setUsersName(String usersName) {
        this.usersName = usersName;
    }

    /**
     * Returns this users full name.
     *
     * @return this users full name.
     */
    private String getUsersName() {
        return this.usersName;
    }

    /**
     * Sets the profile picture of this user.
     *
     * @param image picture of the user to be set.
     */
    public void setUsersPicture(Image image) {
        this.usersPicture = image;
    }

    /**
     * Returns the profile picture of this user.
     *
     * @return profile picture of this user.
     */
    private Image getUsersPicture() {
        return usersPicture;
    }

    /**
     * Sets the stage.
     *
     * @param stage stage to be set.
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
