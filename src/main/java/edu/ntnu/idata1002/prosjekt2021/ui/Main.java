package edu.ntnu.idata1002.prosjekt2021.ui;

import java.io.IOException;
import edu.ntnu.idata1002.prosjekt2021.model.SectionList;
import edu.ntnu.idata1002.prosjekt2021.model.TestData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Starts the application.
 */
public class Main extends Application {

    private static Stage mainStage;

    @Override
    public void start(Stage primaryStage) {
        Parent root = null;

        try {
            SectionList sectionList = new SectionList();
            TestData.hospitalListTestData(sectionList);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Loginpage.fxml"));
            root = loader.load();
            mainStage = primaryStage;
            primaryStage.setTitle("HelseListe");
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/Stylesheets/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the stage used when initilizing the application.
     *
     * @return stage used when initilizing the application.
     */
    public static Stage getStage() {
        return mainStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
