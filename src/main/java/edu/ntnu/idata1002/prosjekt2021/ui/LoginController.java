package edu.ntnu.idata1002.prosjekt2021.ui;

import edu.ntnu.idata1002.prosjekt2021.model.Model;
import edu.ntnu.idata1002.prosjekt2021.model.Nurse;
import edu.ntnu.idata1002.prosjekt2021.model.Section;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;

/**
 * Represents the controller for the login scene.
 *
 * @author gruppe5
 * @version 28.04.2021
 */
public class LoginController {

  /**
   * Textfield for the username for logging in.
   */
  @FXML
  private TextField usernameField;

  /**
   * Passwordfield for logging in.
   */
  @FXML
  private PasswordField passwordField;

  /**
   * Displays errors with login.
   */
  @FXML
  private Label errorLabel;

  /**
   * Authenticates the user, if the user is authenticated the patientpage will display.
   * If not the errorlabel with update.
   */
  @FXML
  private void login() {
    for(Section section : Model.getInstanceOfSectionList().getSections()) {
      for(Nurse nurse : section.getNurses()) {
        //Authenticates
        if(nurse.getUsername().equals(usernameField.getText()) &&
                nurse.getPassword().equals(passwordField.getText())) {

          //Hides the errorlabel.
          errorLabel.setText("");

          Parent root = null;
          try {
            FXMLLoader loader = new FXMLLoader(getClass().
                    getResource("PatientPage.fxml"));
            root = loader.load();
            PatientPageController controller = loader.getController();
            controller.setUserLabel(nurse.getFullName());
            controller.setUserPicture(nurse.getPicture());
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/Stylesheets/application.css").toExternalForm());
            Main.getStage().setScene(scene);
            Main.getStage().sizeToScene();
          } catch (IOException e) {
            e.printStackTrace();
          }
        } else {
          errorLabel.setStyle("-fx-text-fill: red");
          errorLabel.setText("Feil bruker-id eller passord");
        }
      }
    }
  }
}
