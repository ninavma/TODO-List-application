package edu.ntnu.idata1002.prosjekt2021.model;

import javafx.scene.image.Image;


/**
 * Represents dummy entries to test the app.
 */
public class TestData {

    /**
     * Fills the given section list with testdata.
     *
     * @param sectionList section to be filled with testdata.
     */
    public static void hospitalListTestData(SectionList sectionList){

        // Creating patients and nurses.
        Patient patient1 = new Patient("Anne Panne","Borgundvegen 150","+4793204324",
                "020445 12345", "Ålesund", 6011, new Image(getImage("/DummyPatientPhotos/annepanne.png")));
        patient1.setEmergencyContact(new Contact("Hanne Panne", "+4799605346"));
        patient1.setGeneralPractitioner(new Contact("Doktor Brille", "+4770940022"));
        patient1.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient1.setAdditionalInformation("Parkering bak huset \n");
        patient1.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient1.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient1.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient1.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Skyll munn med corsodyl", "Morgen"));
        patient1.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient1.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Morgen"));
        patient1.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Ettermiddag"));
        patient1.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Gåtrening", "Kveld"));
        patient1.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Kveld"));
        patient1.findCareplanByTypeOfCare("Legemidler").addTask(new TodoTask("Paracet", "08:00"));

        Patient patient2 = new Patient("Nils Nesevis","Borgundvegen 103","+4744324402",
                "240739 12346", "Ålesund",6012, new Image(getImage("/DummyPatientPhotos/nilsnesevis.png")));
        patient2.setEmergencyContact(new Contact("Nora Nesevis", "+4790340202"));
        patient2.setGeneralPractitioner(new Contact("Doktor Brille", "+4770940022"));
        patient2.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient2.setAdditionalInformation("Parkering bak huset \n");
        patient2.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient2.setDoCpr(false);
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient2.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient2.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient2.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Skyll munn med corsodyl", "Morgen"));
        patient2.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient2.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Tur", "Ettermiddag"));
        patient2.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Bistå med å ringe datter", "Kveld"));

        Patient patient3 = new Patient("Lars Lapskaus","Hatlevika 35","+4793043423",
                "091241 12347", "Ålesund",6011, new Image(getImage("/DummyPatientPhotos/larslapskaus.png")));
        patient3.setEmergencyContact(new Contact("Lisa Lapskays", "+4794328464"));
        patient3.setGeneralPractitioner(new Contact("Doktor Brille", "+4770940022"));
        patient3.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient3.setAdditionalInformation("Parkering bak huset \n");
        patient3.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient3.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient3.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient3.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient3.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Gåtrening", "Morgen"));
        patient3.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Trappetrening", "Morgen"));
        patient3.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Gåtrening", "Kveld"));
        patient3.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Trappetrening", "Kveld"));

        Patient patient4 = new Patient("Ola Nordmann","Hatlesvingen 25","+4791394342",
            "170132 12348", "Ålesund",6010, new Image(getImage("/DummyPatientPhotos/olanordmann.png")));
        patient4.setEmergencyContact(new Contact("Pia Nordmann", "+4790438243"));
        patient4.setGeneralPractitioner(new Contact("Doktor Brille", "+4770940022"));
        patient4.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient4.setAdditionalInformation("Parkering bak huset \n");
        patient4.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient4.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient4.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient4.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));

        Patient patient5 = new Patient("Kari Nordmann","Indre Hatlen 47","+4743234042",
            "291230 12349", "Ålesund",6010, new Image(getImage("/DummyPatientPhotos/karinordmann.png")));
        patient5.setEmergencyContact(new Contact("Per Nordmann", "+4745045823"));
        patient5.setGeneralPractitioner(new Contact("Doktor Proktor", "+4770945500"));
        patient5.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient5.setAdditionalInformation("Parkering bak huset \n");
        patient5.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient5.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient5.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient5.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Morgen"));
        patient5.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Ettermiddag"));
        patient5.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient5.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Kveld"));
        patient5.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Morgen"));
        patient5.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Ettermiddag"));
        patient5.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Tur", "Kveld"));
        patient5.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Kveld"));

        Patient patient6 = new Patient("Mari Maurskog","Hatlabakken 5","+4743424324",
            "130334 123472", "Ålesund",6011, new Image(getImage("/DummyPatientPhotos/marimaurskog.png")));
        patient6.setEmergencyContact(new Contact("Martin Marihøne", "+4793840234"));
        patient6.setGeneralPractitioner(new Contact("Doktor Proktor", "+4770945500"));
        patient6.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient6.setAdditionalInformation("Parkering bak huset \n");
        patient6.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient6.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient6.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient6.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Morgen"));
        patient6.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Ettermiddag"));
        patient6.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient6.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Kveld"));
        patient6.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Morgen"));
        patient6.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Ettermiddag"));
        patient6.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Tur", "Kveld"));
        patient6.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Kveld"));

        Patient patient7 = new Patient("Solveig Lysgate","Hatlabakken 24","+4795435323",
            "141130 123473", "Ålesund",6011, new Image(getImage("/DummyPatientPhotos/solveiglysgate.png")));
        patient7.setEmergencyContact(new Contact("Luna Lysgate", "+4793849346"));
        patient7.setGeneralPractitioner(new Contact("Doktor Proktor", "+4770945500"));
        patient7.setSummary("Bekkensbrudd, bruker preikestol, ustødig.");
        patient7.setAdditionalInformation("Parkering bak huset \n");
        patient7.setDiagnosis("Bekkenbrudd \nDiabetes type 1");
        patient7.setDoCpr(false);
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient7.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient7.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient7.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Morgen"));
        patient7.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Ettermiddag"));
        patient7.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient7.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Kveld"));
        patient7.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Morgen"));
        patient7.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Ettermiddag"));
        patient7.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Tur", "Kveld"));
        patient7.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Kveld"));

        Patient patient8 = new Patient("Ingrid Langveg","Indre Hatlen 2","+4794342424",
            "120341 123474", "Ålesund",6012, new Image(getImage("/DummyPatientPhotos/ingridlangveg.png")));
        patient8.setEmergencyContact(new Contact("Ingo Langveg", "+4744038324"));
        patient8.setGeneralPractitioner(new Contact("Doktor Proktor", "+4770945500"));
        patient8.setSummary("Bor med mann, trenger bistand til morgen og kveldstell");
        patient8.setAdditionalInformation("Parkering bak huset \n");
        patient8.setDiagnosis("Alzheimers");
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Frokost", "Morgen"));
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Morgen"));
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Mellommåltid", "Ettermiddag"));
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Lunsj", "Ettermiddag"));
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Ettermiddag"));
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("Middag", "Kveld"));
        patient8.findCareplanByTypeOfCare("Ernæring").addTask(new TodoTask("To glass valgfri drikke", "Kveld"));
        patient8.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Morgenstell", "Morgen"));
        patient8.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Morgen"));
        patient8.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Ettermiddag"));
        patient8.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Kveldstell", "Kveld"));
        patient8.findCareplanByTypeOfCare("Personlig hygiene").addTask(new TodoTask("Smøre med canesten", "Kveld"));
        patient8.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Morgen"));
        patient8.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Ettermiddag"));
        patient8.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Tur", "Kveld"));
        patient8.findCareplanByTypeOfCare("Sosial og aktivitet").addTask(new TodoTask("Øvelser fra fysioteraput", "Kveld"));

        Nurse nurse1 = new Nurse("Elisabeth Akerø Modahl", "eliake", "Passord123", new Image(getImage("/UsersPhotos/elisabeth.png")));
        Nurse nurse2 = new Nurse("Victor Modahl", "vicmod", "Passord123", new Image(getImage("/UsersPhotos/victor.png")));
        Nurse nurse3 = new Nurse("Solveig Marstrander","solmar","Passord123", new Image(getImage("/UsersPhotos/nouserpic.png")));
        Nurse nurse4 = new Nurse("Silje Henningsen","silhen","Passord123", new Image(getImage("/UsersPhotos/nouserpic.png")));
        Nurse nurse5 = new Nurse("Jeevana Arul", "jeearu", "Passord123", new Image(getImage("/UsersPhotos/nouserpic.png")));
        Nurse nurse6 = new Nurse("Janita Lillevik Røyseth", "janroy", "Passord123", new Image(getImage("/UsersPhotos/janita.png")));
        Nurse nurse7 = new Nurse("Nina Vinding Marstrander", "ninmar", "Passord123", new Image(getImage("/UsersPhotos/nina.jpeg")));
        Nurse nurse8 = new Nurse("Winnie Wu", "winwu", "Passord123", new Image(getImage("/UsersPhotos/winnie.png")));
        Nurse nurse9 = new Nurse("Balendra Sounthararajan", "balsou", "Passord123", new Image(getImage("/UsersPhotos/balendra.png")));
        Nurse nurse10 = new Nurse("Kjell Inge Tomren", "kjetom", "Passord123", new Image(getImage("/UsersPhotos/kjellinge.jpeg")));

        // Creating section and adding patients and nurses.
        Section hatlane = new Section("Hatlane");
        sectionList.addSection(hatlane);
        hatlane.addNurse(nurse1);
        hatlane.addNurse(nurse2);
        hatlane.addNurse(nurse3);
        hatlane.addNurse(nurse4);
        hatlane.addNurse(nurse5);
        hatlane.addNurse(nurse6);
        hatlane.addNurse(nurse7);
        hatlane.addNurse(nurse8);
        hatlane.addNurse(nurse9);
        hatlane.addNurse(nurse10);
        hatlane.addPatient(patient1);
        hatlane.addPatient(patient2);
        hatlane.addPatient(patient3);
        hatlane.addPatient(patient4);
        hatlane.addPatient(patient5);
        hatlane.addPatient(patient6);
        hatlane.addPatient(patient7);
        hatlane.addPatient(patient8);
    }

    private static String getImage(String path) {
        return TestData.class.getResource(path).toExternalForm();
    }
}
