package edu.ntnu.idata1002.prosjekt2021.model;

import javafx.scene.control.CheckBox;

/**
 * Represents a task. Holds a checkbox and a specified time of day for
 * the tasks "deadline".
 *
 * @author gruppe5
 * @version 24.04.2021
 */
public class TodoTask {

    /**
     * The checkbox belonging to this todotask.
     */
    private CheckBox checkBox;

    /**
     * The time of the when this todotask should be done.
     */
    private String timeOfDay;

    /**
     * Creates an instance of todotask.
     *
     * @param description description of task.
     * @param timeOfDay the time of day when the task is to be done.
     */
    public TodoTask(String description, String timeOfDay) {
        this.checkBox = new CheckBox(description);
        this.timeOfDay = timeOfDay;
    }

    /**
     * Returns the checkbox this todotask holds on.
     *
     * @return checkbox this todotask holds on.
     */
    public CheckBox getCheckbox() {
        return this.checkBox;
    }

    /**
     * Sets the description of the todotask.
     *
     * @param description description of the todotask to be set.
     */
    public void setDescription(String description) {
        this.checkBox.setText(description);
    }

    /**
     * Returns the time of day when this todotask should be done.
     *
     * @return time of day when this todotask should be done.
     */
    public String getTimeOfDay() {
        return this.timeOfDay;
    }

    /**
     * Sets the time of day when this todotask should be done.
     *
     * @param timeOfDay time of day this todotask should be done.
     */
    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    /**
     * Returns if this todotask is done or not.
     *
     * @return <code>true</code> if the todotask is done.
     * <code>false</code> if the todotask is not done.
     */
    public boolean isDone() {
        return this.checkBox.isSelected();
    }
}
