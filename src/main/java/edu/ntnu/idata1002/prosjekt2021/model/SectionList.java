package edu.ntnu.idata1002.prosjekt2021.model;

import java.util.ArrayList;
import edu.ntnu.idata1002.prosjekt2021.exceptions.RemoveException;

/**
 * Represents a list over sections.
 *
 * @author gruppe 5
 * @version 18.03.2021
 */
public class SectionList {

  /**
   * List over sections in this sectiolist.
   */
  private ArrayList<Section> sections;

  /**
   * Creates a new instance of sectionlist.
   */
  public SectionList() {
    this.sections = new ArrayList<>();
  }

  /**
   * Adds a new section to the section list.
   *
   * @param section section to be added to the section list.
   */
  public void addSection(Section section) {
    if( section == null ){
      throw new IllegalArgumentException("Sorry, the section cannot be null");
    }

    if (this.sections.contains( section )) {
      throw new IllegalArgumentException("The section you are trying to add is already in the list");
    } else {
      this.sections.add( section );
    }
  }

  /**
   * Removes a section from the section list.
   *
   * @param section section to be removed from the section list.
   * @throws RemoveException if the section did not get removed.
   */
  public void removeSection(Section section) throws RemoveException {
    if (sections.contains( section )) {
      this.sections.remove( section );
    } else {
      throw new RemoveException( "The section did not get removed" );
    }
  }

  /**
   * Iterates over the sectionlist and returns the first section
   * with the name given.
   *
   * @param sectionName name of the section to search for.
   * @return section with the matching name, null if no section
   * matches.
   */
  public Section findSectionBySectionName(String sectionName) {
    Section sectionFound = null;

    for(Section section : this.getSections()) {
      if(sectionName.equals(section.getSectionName())) {
        sectionFound = section;
      }
    }

    return sectionFound;
  }

  /**
   * Returns a list with all the sections.
   *
   * @return a list with all the sections.
   */
  public ArrayList<Section> getSections() {
    return this.sections;
  }
}
