package edu.ntnu.idata1002.prosjekt2021.model;

/**
 * Class used for implementing the test data to the application.
 */
public class Model {

    private static SectionList sectionList;

    /**
     *
     */
    private Model() {

    }

    /**
     * Returns instance of sectionList, if no instance exist and
     * instance will be created and returned.
     *
     * @return instance of sectionlist.
     */
    public static SectionList getInstanceOfSectionList() {
        if(sectionList == null) {
            sectionList = new SectionList();
            TestData.hospitalListTestData(sectionList);
        }
        return sectionList;
    }
}
