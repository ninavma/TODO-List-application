package edu.ntnu.idata1002.prosjekt2021.model;

import javafx.scene.image.Image;

/**
 * Represents a nurse with unique id.
 */
public class Nurse {

    /**
     * This nurses full name.
     */
    private String fullName;

    /**
     * This nurses username.
     */
    private String username;

    /**
     * This nurses password.
     */
    private String password;

    /**
     * This nurses profilepicture
     */
    private Image picture;


    /**
     * Creates an instance of nurse.
     *
     * @param fullName  full name of a nurse.
     * @param username unique id of a nurse.
     * @param password password for the nurses user.
     * @param picture the profile picture of the nurse.
     */
    public Nurse(String fullName, String username, String password, Image picture){
        if(fullName == null || fullName.isBlank() || username == null ||
                username.isBlank() || password == null || password.isBlank()) {
            throw new IllegalArgumentException("Name, id, password or tlf cannot be empty or null");
        }
        this.fullName = fullName;
        this.password = password;
        this.username = username;
        this.picture = picture;
    }

    /**
     * Returns this nurses full name.
     *
     * @return this nurses full name.
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Returns this nurses username.
     *
     * @return this nurses username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns this nurses password.
     *
     * @return this nurses password.
     */
    public String getPassword(){
        return password;
    }

    /**
     * Returns this nurses profile picture.
     *
     * @return this nurses profile picture.
     */
    public Image getPicture() {
        return this.picture;
    }

}
