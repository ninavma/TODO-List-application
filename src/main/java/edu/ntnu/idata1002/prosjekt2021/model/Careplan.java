package edu.ntnu.idata1002.prosjekt2021.model;

import javafx.scene.control.CheckBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a patients careplan. Contains the specified type
 * of care in the plan. Holds on list over the tasks associated with
 * this type of care, and a list over journal entries associated with
 * this type of care.
 *
 * @author gruppe 5
 * @version 24.04.2021
 */

public class Careplan {

    /**
     * The type of care the tasks in this careplan is about.
     */
    private String typeOfCare;

    /**
     * List of tasks in this careplan.
     */
    private ArrayList<TodoTask> tasks;

    /**
     * List over journal entrys in this careplan.
     */
    private ArrayList<JournalEntry> journal;

    /**
     * Creates an instance of careplan.
     *
     * @param typeOfCare the type of cate of the careplan instance.
     */
    public Careplan(String typeOfCare){
        this.typeOfCare = typeOfCare;
        tasks = new ArrayList<>();
        journal = new ArrayList<>();
    }

    /**
     * Adds a task to the careplan.
     *
     * @param todoTask task to be added to careplan.
     */
    public void addTask(TodoTask todoTask) {
        this.tasks.add(todoTask);
    }

    /**
     * Removes task from the careplan.
     *
     * @param todoTask task to be removed from careplan.
     */
    public void removeTask(TodoTask todoTask) {
        this.tasks.remove(todoTask);
    }

    /**
     * Changes the position of the given todotask with one spot down.
     *
     * @param todoTask the task to be moved up in the list
     */
    public void changeTaskPositionUp(TodoTask todoTask) {
        if(this.tasks.indexOf( todoTask ) == 0) {
            //Do nothing
        } else {
            TodoTask todoTaskMovedDown = this.tasks.get(this.tasks.indexOf( todoTask )-1);
            int todoTaskIndex = this.tasks.indexOf(todoTask);
            this.tasks.set( this.tasks.indexOf( todoTask )-1, todoTask);
            this.tasks.set( todoTaskIndex, todoTaskMovedDown);
        }
    }

    /**
     * Changes the position of the given todoTask with one spot up.
     *
     * @param todoTask todotask to be places one spot up.
     */
    public void changeTaskPositionDown(TodoTask todoTask) {
        if(this.tasks.indexOf( todoTask ) == this.tasks.size()-1) {
            //Do nothing
        } else {
            TodoTask todoTaskMovedUp = this.tasks.get(this.tasks.indexOf( todoTask ) + 1);
            int todoTaskIndex = this.tasks.indexOf(todoTask);
            this.tasks.set( this.tasks.indexOf( todoTask ) + 1, todoTask);
            this.tasks.set( todoTaskIndex, todoTaskMovedUp);
        }
    }

    /**
     * Changes the position of the given to do task to the
     * bottom of the ArrayList.
     *
     * @param todoTask todoTask to be placed at the bottom.
     */
    public void changeTaskPostitionBottom(TodoTask todoTask) {
        if(this.tasks.indexOf( todoTask ) == this.tasks.size()-1) {
            //Do nothing
        } else {
            for(TodoTask task : this.tasks) {
                if(this.tasks.indexOf(task) > this.tasks.indexOf(todoTask)) {
                    this.tasks.set(this.tasks.indexOf(task)-1, task);
                }
            }
            this.tasks.set( this.tasks.size()-1, todoTask);
        }
    }

    /**
     * Changes the position of the given todoTask to the
     * top of the ArrayList.
     *
     * @param todoTask todoTask to be places at top.
     */
    public void changeTaskPostitionTop(TodoTask todoTask) {
        this.tasks.remove(todoTask);
        this.tasks.add(0, todoTask);
    }

    /**
     * Iterates over the list of todotasks and returns the todotask
     * with the matching checkbox. If no matches are found, null will
     * be returned.
     *
     * @param checkBox the checkbox to find to the corresponding todotask item for.
     * @return todotask with the corresponding checkbox, or null if no matches are found.
     */
    public TodoTask findTaskByCheckBox(CheckBox checkBox) {
        TodoTask todoTaskFound = null;

        for(TodoTask todoTask : this.tasks) {
            if(checkBox.equals(todoTask.getCheckbox())) {
                todoTaskFound = todoTask;
            }
        }

        return todoTaskFound;
    }

    /**
     * Returns the type of care in this careplan.
     *
     * @return type of care in this careplan.
     */
    public String getTypeOfCare() {
        return this.typeOfCare;
    }

    /**
     * Returns the list of tasks in this careplan.
     *
     * @return list of tasks in this careplan.
     */
    public List<TodoTask> getTasks() {
        return this.tasks;
    }

    /**
     * Returns a list over tasks in this careplan with the deadline of
     * the given time of day.
     *
     * @param timeOfDay the time of day of the tasks to find.
     * @return list over tasks in this careplan to be done at the specified time of day.
     */
    public List<TodoTask> getTasksByTimeOfDay(String timeOfDay) {
        List<TodoTask> tasksByTimeOfDay = new ArrayList<>();
        for (TodoTask task : tasks) {
            if (task.getTimeOfDay().equals(timeOfDay)) {
                tasksByTimeOfDay.add(task);
            }
        }
        return tasksByTimeOfDay;
    }

    /**
     * Adds a journal entry to the journal.
     *
     * @param journalEntry the entry to be added to the journal.
     */
    public void addJournalEntry(JournalEntry journalEntry) {
        this.journal.add(journalEntry);
    }

    /**
     * Returns the journal(list) that cointains the journal entries.
     *
     * @return journal(list) that contains the journal entries.
     */
    public List<JournalEntry> getJournal() {
        return this.journal;
    }

}
