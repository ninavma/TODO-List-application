package edu.ntnu.idata1002.prosjekt2021.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents a journal entry. Holds information about the time
 * of the journal entry and the text in the journal entry.
 */
public class JournalEntry {

    /**
     * The date of when this journal entry was made.
     */
    private final LocalDateTime entryTime;

    /**
     * The user who made the journal entry.
     */
    private final String usersName;

    /**
     * The journal entry.
     */
    private final String journalEntry;

    /**
     * Creates an instance of journal entry.
     *
     * @param journalEntry the text in the journal entry.
     * @param usersName the name of the user who made the entry.
     */
    public JournalEntry(String journalEntry, String usersName) {
        this.entryTime = LocalDateTime.now();
        this.journalEntry = journalEntry;
        this.usersName = usersName;
    }

    /**
     * Returns the time of the entry.
     *
     * @return time for when this jorunal entry was created.
     */
    public String getEntryTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

        String formatDateTime = entryTime.format(formatter);

        return formatDateTime;
    }

    /**
     * Returns the text in this jounral entry.
     *
     * @return text in thos journal entry.
     */
    public String getJournalEntry() {
        return this.journalEntry;
    }

    /**
     * Returns the name of the user who made this journal entry.
     *
     * @return name of the user who made this journal entry.
     */
    public String getUsersName() {
        return this.usersName;
    }
}
