package edu.ntnu.idata1002.prosjekt2021.model;
import java.util.ArrayList;
import java.util.List;
import edu.ntnu.idata1002.prosjekt2021.exceptions.RemoveException;

/**
 * Represents a section.
 * A section is based on postalcodes.
 * Holds a list of patients and nurses.
 *
 * @author gruppe 5
 * @version 23.04.2021
 */
public class Section {

    /**
     * The name of this section.
     */
    private String sectionName;

    /**
     * List over postalcodes this section covers.
     */
    private ArrayList<Integer> postalCode;

    /**
     * List over patients in this section.
     */
    private ArrayList<Patient> patients;

    /**
     * List over nurses working in this section.
     */
    private ArrayList<Nurse> nurses;

    /**
     * Creates a instance of Section.
     *
     * @param sectionName name of the section.
     */
    public Section(String sectionName){
        if(sectionName == null){
            sectionName = "";
        }

        if(sectionName.isBlank()){
            throw new IllegalArgumentException("Section number cannot be empty or null");
        }

        this.sectionName = sectionName;
        this.patients = new ArrayList<>();
        this.nurses = new ArrayList<>();
        this.postalCode = new ArrayList<>();
    }

    /**
     * Returns this section name.
     *
     * @return this section name.
     */
    public String getSectionName() {

        return sectionName;
    }

    /**
     * Returns postcodes covered by a section.
     *
     * @return postcodes covered by a section.
     */
    public ArrayList<Integer> getPostalCode() {
        return postalCode;
    }

    /**
     * Sets new section name.
     *
     * @param sectionName the new section name to be set.
     */
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    /**
     * Sets a list of new postcodes to a section.
     *
     * @param postalCode new postcode to a section.
     */
    public void setPostalCode(ArrayList<Integer> postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Adding a new patient to a list of patients.
     * Only a patient with unique socialSecurityNumber will be added.
     *
     * @param patient a new patient.
     */
    public void addPatient(Patient patient) {

        if (patient == null) {
            throw new IllegalArgumentException("Sorry, the patient could not be added");
        }
        if (this.patients.contains(patient)) {
            throw new IllegalArgumentException("Sorry, the patient is already in the register");
        } else {
            this.patients.add(patient);
        }
        this.postalCode.add(patient.getPostalCode());
    }

    /**
     * Adding a given nurse to a list of nurses.
     *
     * @param nurse the nurse to be added to the list of nurses..
     */
    public void addNurse(Nurse nurse) throws IllegalArgumentException{
        if(nurse == null){
            throw new IllegalArgumentException("Sorry, nurse can not be null");
        }
        if(this.nurses.contains((nurse))) {
            throw new IllegalArgumentException("Sorry, the nurse is already in the register");
        } else {
            this.nurses.add(nurse);
        }
    }

    /**
     * Removing a patient, if patient is in the list.
     *
     * @param patient patient to be removed.
     * @throws RemoveException if the patient was not removed
     */
    public void removePatient(Patient patient) throws RemoveException {
        if(patients.contains(patient)) {
            this.patients.remove(patient);
        }
        else{
            throw new RemoveException("Someone did not get removed");
        }
    }

    /**
     * Removing a nurse, if nurse is in the list.
     *
     * @param nurse nurse to be removed.
     * @throws RemoveException if the nurse was not removed.
     */
    public void removeNurse(Nurse nurse) throws RemoveException{
        if(nurses.contains(nurse)) {
            this.nurses.remove(nurse);
        }
        else{
            throw new RemoveException("Someone did not get removed");
        }
    }

    /**
     * Returns a list with all the patients.
     *
     * @return a list with all the patients.
     */
    public List<Patient> getPatients()
    {
        return patients;
    }

    /**
     * Returns a list with all the nurses.
     *
     * @return a list with all the nurses.
     */
    public List<Nurse> getNurses()
    {
        return nurses;
    }
}
