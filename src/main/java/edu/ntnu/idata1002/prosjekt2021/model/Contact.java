package edu.ntnu.idata1002.prosjekt2021.model;

/**
 * Represents a contact person. Holds the contacts name and phonenumber.
 */
public class Contact {

    /**
     * The contact persons full name.
     */
    private String fullName;

    /**
     * The contact persons phonenumber.
     */
    private String phonenumber;

    /**
     * Creates an instance of a Contact.
     *
     * @param fullName the contact persons full name.
     * @param phonenumber the contact persons phonenumber.
     */
    public Contact(String fullName, String phonenumber) {
        this.fullName = fullName;
        this.phonenumber = phonenumber;
    }

    /**
     * Returns this contact persons full name.
     *
     * @return this contact persons full name.
     */
    public String getFullName() {
        return this.fullName;
    }

    /**
     * Sets this contact persons full name.
     *
     * @param fullName the full name to be set to this contact person.
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Returns this contact persons phonenumber.
     *
     * @return this contact persons phonenumber.
     */
    public String getPhonenumber() {
        return this.phonenumber;
    }

    /**
     * Sets this contact persons phonenumber.
     *
     * @param phonenumber phonenumber to be set to this contact person.
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
