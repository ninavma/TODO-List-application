package edu.ntnu.idata1002.prosjekt2021.model;

import javafx.scene.image.Image;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Patient.
 * Uniquely identified by socialSecurityNumber.
 *
 * @author gruppe 5
 * @version 23.04.2021
 */
public class Patient {

    /**
     * This patients full name.
     */
    private String fullName;

    /**
     * This patients address.
     */
    private String address;

    /**
     * This patients socialSecurityNumber;
     */
    private final String socialSecurityNumber;

    /**
     * This patients phonenumber.
     */
    private String phonenumber;


    /**
     * The city where this patient lives.
     */
    private String city;

    /**
     * The postal code where this patient resides.
     */
    private int postalCode;

    /**
     * The picture of this patient.
     */
    private Image picture;

    /**
     * Contact information about the patients emergency contact.
     */
    private Contact emergencyContact;

    /**
     * Contact information about the patients general practitioner.
     */
    private Contact generalPractitioner;

    /**
     * A short summary of the extent of care the patient needs.
     */
    private String summary;

    /**
     * Relevant information for nurse about parking, postbox number, codes,
     * anything deemed necessary for providing the their patients needs.
     */
    private String additionalInformation;

    /**
     * Patients diagnosis.
     */
    private String diagnosis;

    /**
     * If the patient wished do be saved.
     */
    private boolean doCpr;

    /**
     * The list of careplans for this patients care.
     */
    private ArrayList<Careplan> careplans;

    /**
     * Creates an instance of a patient.
     *
     * @param fullName full name of the patient.
     * @param address address of the patient.
     * @param phonenumber telephone number of the patient.
     * @param socialSecurityNumber unique socialSecurityNumber of the patient.
     * @param city the city where this patient resides.
     * @param postalCode the postal code where this patient resides.
     * @param picture picture of the patient.
     */
    public Patient(String fullName, String address, String phonenumber,
                   String socialSecurityNumber, String city, int postalCode, Image picture){
       //These fields are required, other fields can be set later.
       if(fullName == null || fullName.isBlank() ||
               socialSecurityNumber == null || socialSecurityNumber.isBlank()){
           throw new IllegalArgumentException("Name and social security number can not be empty");
       }
       this.fullName = fullName;
       this.address = address;
       this.phonenumber = phonenumber;
       this.socialSecurityNumber = socialSecurityNumber;
       this.postalCode = postalCode;
       this.city = city;
       this.picture = picture;
       this.doCpr = true;
       this.careplans = new ArrayList<>();
       this.careplans.add(new Careplan("Personlig hygiene"));
       this.careplans.add(new Careplan("Ernæring"));
       this.careplans.add(new Careplan("Sosial og aktivitet"));
       this.careplans.add(new Careplan("Legemidler"));
    }

    /**
     * Returns the full name of the patient.
     *
     * @return full name of the patient.
     */
    public String getFullName() {

        return fullName;
    }

    /**
     * Sets new address to the patient.
     *
     * @param address new address of the patient
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Returns the address of the patient.
     *
     * @return address of the patient.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Returns the telephone number of the patient.
     *
     * @return telephone number of the patient.
     */
    public String getPhonenumber() {
        return phonenumber;
    }

    /**
     * Returns the socialSecurityNumber of the patient.
     *
     * @return socialSecurityNumber of the patient.
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Sets new telephone number to the patient.
     *
     * @param phonenumber new telephone number of the patient.
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    /**
     * Returns the city of where this patient lives.
     *
     * @return city where this patient lives.
     */
    public String getCity() {
        return this.city;
    }

    /**
     * Returns the postalcode of where this patient lives.
     *
     * @return postalcode of where this patient lives.
     */
    public int getPostalCode() {
      return postalCode;
    }

    /**
     * Sets the picture of this patient.
     *
     * @param picture picture of this patient.
     */
    public void setPicture(Image picture) {
        this.picture = picture;
    }

    /**
     * Returns picture of this patient.
     *
     * @return picture of this patient.
     */
    public Image getPicture() {
        return this.picture;
    }

    /**
     * Returns this patients emergency contact.
     *
     * @return this patients emergency contact.
     */
    public Contact getEmergencyContact() {
        return this.emergencyContact;
    }

    /**
     * Sets this patients emergency contact.
     *
     * @param emergencyContact emergency contact to be set.
     */
    public void setEmergencyContact(Contact emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    /**
     * Returns this patients general practitioner.
     *
     * @return this patients general practitioner.
     */
    public Contact getGeneralPractitioner() {
        return this.generalPractitioner;
    }

    /**
     * Sets this patients general practitioner.
     *
     * @param generalPractitioner general practitioner to be set.
     */
    public void setGeneralPractitioner(Contact generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Returns the short summary about the patient's condition and
     * what extent of care is needed from them.
     *
     * @return summary about the extent of care needed.
     */
    public String getSummary() {
        return this.summary;
    }

    /**
     * Sets the short summary about the extent of care the patient needs.
     *
     * @param summary summary of the extent of care the patient needs to be set.
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * Returns additional information about the patient.
     *
     * @return additional information about the patient.
     */
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Sets additional information about the patient.
     *
     * @param additionalInformation additional information about the patient to be set.
     */
    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    /**
     * Returns this patients current diagnosis.
     *
     * @return this patients current diagnosis.
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets this patients current diagnosis.
     * @param diagnosis this patients current diagnosis to be set.
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Add careplan to the patients list of careplans.
     *
     * @param careplan the careplan to be added.
     */
    public void addCareplan(Careplan careplan) {
        this.careplans.add(careplan);
    }

    /**
     * Removes the given careplan from the patients list of careplans.
     *
     * @param careplan the careplan to be removed.
     */
    public void removeCareplan(Careplan careplan) {
        this.careplans.remove(careplan);
    }

    /**
     * Finds the careplan mathching given the type of care.
     *
     * @param typeOfCare the type of care in the careplan to look for.
     * @return careplan the careplan with the specified type of care. Returns null
     * if no matching careplan is found.
     */
    public Careplan findCareplanByTypeOfCare(String typeOfCare) {
        Careplan careplanToBeFound = null;

        for(Careplan careplan : this.careplans) {
            if(careplan.getTypeOfCare().equals(typeOfCare)) {
                careplanToBeFound = careplan;
            }
        }

        return careplanToBeFound;
    }

    /**
     * Returns this patients list of careplans.
     *
     * @return list of this patients careplans.
     */
    public List<Careplan> getCareplans() {
        return this.careplans;
    }

    /**
     * Sets whether the patient wishes to be saved or not.
     *
     * @param doCpr <code>true</code> if the patient wishes to be saved.
     *              <code>false</code> if the patient do not want to be saved.
     */
    public void setDoCpr(boolean doCpr) {
        this.doCpr = doCpr;
    }

    /**
     * Returns whether the patient whishes to be saved or not.
     *
     * @return <code>true</code> if the patient wishes to be saved.
     *         <code>false</code> if the patient do not want to be saved.
     */
    public boolean doCpr() {
        return this.doCpr;
    }
}
